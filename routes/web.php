<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*---------------------------Front End---------------------------*/

Route::get('/', 'FrontEndController@index')->name('home');                                        // INDEX
Route::get('/contact', 'FrontEndController@contact');                               // CONTACT US

//for register and login user

Route::group(['prefix' => 'user/', 'as' => 'frontend.user.' ], function (){
    Route::get('register',                ['as' => 'register',               'uses' => 'FrontendUserController@register']);
    Route::post('update/{user}',                    ['as' => 'update',                'uses' => 'FrontendUserController@update']);
    Route::get('login',                    ['as' => 'login',                'uses' => 'FrontendUserController@register']);
    Route::post('login',                    ['as' => 'login',                'uses' => 'FrontendUserController@login']);
    Route::post('store',              ['as' => 'store',                  'uses' => 'FrontendUserController@store']);
    Route::get('logout',            ['as' => 'logout',               'uses' => 'FrontendUserController@logout']);
});

//for register and login admin

Route::group(['prefix' => 'admin/', 'as' => 'backend.admin.' ], function (){
    Route::get('register',                ['as' => 'register',               'uses' => 'BackendAdminController@register']);
    Route::get('view',                ['as' => 'view',               'uses' => 'BackendAdminController@view']);
    Route::get('login',                    ['as' => 'login.form',                'uses' => 'BackendAdminController@login_form']);
    Route::post('login',                    ['as' => 'login',                'uses' => 'BackendAdminController@login']);
    Route::post('store',              ['as' => 'store',                  'uses' => 'BackendAdminController@store']);
    Route::get('logout',            ['as' => 'logout',               'uses' => 'BackendAdminController@logout']);
    Route::delete('destroy/{admin}',   ['as' => 'destroy',               'uses' => 'BackendAdminController@destroy']);
});


//for brand in backend
Route::group(['prefix' => 'brand/', 'as' => 'backend.brand.','middleware'=> 'auth:admin' ], function (){
    Route::get('create',                ['as' => 'create',               'uses' => 'BrandController@create']);
    Route::post('store',                    ['as' => 'store',                'uses' => 'BrandController@store']);
    Route::get('view',              ['as' => 'view',                  'uses' => 'BrandController@view']);
    Route::delete('destroy/{manufacturer}',            ['as' => 'destroy',               'uses' => 'BrandController@destroy']);
});

//for category in backend
Route::group(['prefix' => 'category/', 'as' => 'backend.category.','middleware'=> 'auth:admin' ], function (){
    Route::get('create',                ['as' => 'create',               'uses' => 'CategoryController@create']);
    Route::post('store',                    ['as' => 'store',                'uses' => 'CategoryController@store']);
    Route::get('view',              ['as' => 'view',                  'uses' => 'CategoryController@view']);
    Route::delete('destroy/{category}',            ['as' => 'destroy',               'uses' => 'CategoryController@destroy']);
});

//for cart in frontend
Route::group(['prefix' => 'cart/', 'as' => 'frontend.cart.' ], function (){
    //Route::get('create',                ['as' => 'create',               'uses' => 'CategoryController@create']);
    Route::post('store',                  ['as' => 'store',                'uses' => 'CartController@store']);
    Route::post('change/quantity',        ['as' => 'change.quantity',      'uses' => 'CartController@changeQuantity']);
    Route::get('view',                    ['as' => 'view',                  'uses' => 'CartController@view']);
    Route::get('destroy/{cart}',         ['as' => 'destroy',               'uses' => 'CartController@destroy']);
});

Route::post('/customer/order','OrderController@order')->name('frontend.user.order');
Route::get('admin/order','OrderController@view')->name('backend.order.view');
Route::get('admin/order/{order}','OrderController@destroy')->name('backend.order.destroy');


Route::get('/customer-account', 'FrontEndController@customerAccount');              // CUSTOMER ACCOUNT

Route::get('/customer-order', 'FrontEndController@customerOrder');                  // CUSTOMER ORDER DETAIL
Route::get('/customer-orders', 'FrontEndController@customerOrders');                // CUSTOMER ORDERS
Route::get('/customer-wishlist', 'FrontEndController@customerWishlist');            // CUSTOMER WISHLIST

Route::get('/checkout1', 'FrontEndController@checkout1')->name('frontend.user.checkout1');                           // CHECKOUT STEP 1
Route::get('/checkout2', 'FrontEndController@checkout2')->name('frontend.user.checkout2');                           // CHECKOUT STEP 2
Route::get('/checkout3', 'FrontEndController@checkout3')->name('frontend.user.checkout3');                           // CHECKOUT STEP 3
Route::get('/checkout4', 'FrontEndController@checkout4')->name('frontend.user.checkout4');                           // CHECKOUT STEP 4

Route::resource('products', 'ProductsController');                              // Products
Route::get('product/{slug}',['as' => 'product.single',
    'uses' => 'FrontEndController@getSingle'])->where('slug','[\w\d\-]+');      //single product using slug

                                /*----------- For Men Clothing -----------*/
Route::get('/mens-tshirt', 'FrontEndController@menTshirt');                             // Men's T-Shirt
Route::get('/mens-shirt', 'FrontEndController@menShirt');                               // Men's Shirt
Route::get('/mens-pant', 'FrontEndController@menPant');                                 // Men's Pant
Route::get('/mens-coat', 'FrontEndController@menCoat');                                 // Men's Coat
Route::get('/mens-windcheater', 'FrontEndController@menWindCheater');                   // Men's Wind Cheater
Route::get('/mens-jacket', 'FrontEndController@menJacket');                             // Men's Jacket
Route::get('/mens-hoodies', 'FrontEndController@menHoddies');                           // Men's Hoodies
Route::get('/mens-shorts', 'FrontEndController@menShorts');                             // Men's Shorts
Route::get('/mens-socks', 'FrontEndController@menSocks');                               // Men's Socks
                                /*----------- For Men Shoes -----------*/
Route::get('/mens-sport-shoes', 'FrontEndController@menSportShoes');                    // Men's Sport Shoes
Route::get('/mens-casual-shoes', 'FrontEndController@menCasualShoes');                  // Men's Casual Shoes
Route::get('/mens-formal-shoes', 'FrontEndController@menFormalShoes');                  // Men's Formal Shoes
Route::get('/mens-boots', 'FrontEndController@menBoots');                               // Men's boots
Route::get('/mens-sandals-slippers', 'FrontEndController@menSandalsAndSlippers');       // Men's Sandals N Slippers
                                /*----------- For Men Accessories -----------*/
Route::get('/mens-bags', 'FrontEndController@menBags');                                 // Men's Bags
Route::get('/mens-cap-hat-gloves', 'FrontEndController@menCapHatGlove');                // Men's Cap Hat Glove
Route::get('/mens-goggles', 'FrontEndController@menGoggles');                           // Men's Gogs
                                /*----------- For Women Clothing -----------*/
Route::get('/womens-tshirt', 'FrontEndController@womenTshirt');                             // Women's T-Shirt
Route::get('/womens-shirt', 'FrontEndController@womenShirt');                               // Women's Shirt
Route::get('/womens-pant', 'FrontEndController@womenPant');                                 // Women's Pant
Route::get('/womens-coat', 'FrontEndController@womenCoat');                                 // Women's Coat
Route::get('/womens-windcheater', 'FrontEndController@womenWindCheater');                   // Women's Wind Cheater
Route::get('/womens-jacket', 'FrontEndController@womenJacket');                             // Women's Jacket
Route::get('/womens-hoodies', 'FrontEndController@womenHoddies');                           // Women's Hoodies
Route::get('/womens-shorts', 'FrontEndController@womenShorts');                             // Women's Shorts
Route::get('/womens-socks', 'FrontEndController@womenSocks');                               // Women's Socks
                                /*----------- For Women Shoes -----------*/
Route::get('/womens-boots', 'FrontEndController@womenBoots');                               // Women's Boots
Route::get('/womens-sport-shoes', 'FrontEndController@womenSportShoes');                    // Women's Sport Shoes
Route::get('/womens-casual-shoes', 'FrontEndController@womenCasualShoes');                  // Women's Casual Shoes
Route::get('/womens-formal-shoes', 'FrontEndController@womenFormalShoes');                  // Women's Formal Shoes
Route::get('/womens-hiking-shoes', 'FrontEndController@womenHikingShoes');                  // Women's Hiking Shoes
Route::get('/womens-heels', 'FrontEndController@womenHeels');                               // Women's Heels
Route::get('/womens-flats-sandals', 'FrontEndController@womenSandalsAndSlippers');          // Women's Sandals N Flats
                                /*----------- For Women Accessories -----------*/
Route::get('/womens-bags', 'FrontEndController@womenBags');                                 // Women's Bags
Route::get('/womens-cap-hat-gloves', 'FrontEndController@womenCapHatGlove');                // Women's Cap Hat Glove
Route::get('/womens-goggles', 'FrontEndController@womenGoggles');                           // Women's Gogs
                                /*----------- For Kid Clothing -----------*/
Route::get('/kids-tshirt', 'FrontEndController@kidsTshirt');                             // Men's T-Shirt
Route::get('/kids-shirt', 'FrontEndController@kidsShirt');                               // Men's Shirt
Route::get('/kids-pant', 'FrontEndController@kidsPant');                                 // Men's Pant
Route::get('/kids-jacket', 'FrontEndController@kidsJacket');                             // Men's Jacket
Route::get('/kids-shorts', 'FrontEndController@kidsShorts');                             // Men's Shorts
Route::get('/kids-socks', 'FrontEndController@kidsSocks');                               // Men's Socks
                                /*----------- For Kid Shoes -----------*/
Route::get('/kids-boots', 'FrontEndController@kidsBoots');                               // Men's boots
Route::get('/kids-sport-shoes', 'FrontEndController@kidsSportShoes');                    // Men's Sport Shoes
Route::get('/kids-casual-shoes', 'FrontEndController@kidsSchoolShoes');                  // Men's School Shoes
Route::get('/kids-sandals-slippers', 'FrontEndController@kidsSandalsAndSlippers');       // Men's Sandals N Slippers
                                /*----------- For Kid Accessories -----------*/
Route::get('/kids-bags', 'FrontEndController@kidsBags');                                 // Men's Bags
Route::get('/kids-cap-hat-gloves', 'FrontEndController@kidsCapHatGlove');                // Men's Cap Hat Glove
Route::get('/kids-goggles', 'FrontEndController@kidsGoggles');                           // Men's Gogs
/*---------------------------Back End---------------------------*/

Route::get('/dashboard', 'BackEndController@index')->name('dashboard');                                        // INDEX
Route::resource('backendproducts', 'BackEndProductsController');                              // Products
Route::get('admin/product/{slug}',['as' => 'backendproduct.single',
    'uses' => 'BackEndController@getSingle'])->where('slug','[\w\d\_]+');      //single product using slug


 
Route::group(['prefix' => 'backend/', 'as' => 'backend.size.','middleware'=> 'auth:admin' ], function (){
    Route::get('size',                    ['as' => 'view',               'uses' => 'SizeController@view']);
    Route::get('size/create',             ['as' => 'create',         'uses' => 'SizeController@create']);
    Route::post('size/store',              ['as' => 'store',               'uses' => 'SizeController@store']);
    Route::delete('size/destroy/{size}',   ['as' => 'destroy',               'uses' => 'SizeController@destroy']);
});   

Route::group(['prefix' => 'backend/', 'as' => 'backend.color.','middleware'=> 'auth:admin' ], function (){
    Route::get('color',                       ['as' => 'view',               'uses' => 'ColorController@view']);
    Route::get('color/create',                ['as' => 'create',         'uses' => 'ColorController@create']);
    Route::post('color/store',                 ['as' => 'store',               'uses' => 'ColorController@store']);
    Route::delete('color/destroy/{color}',     ['as' => 'destroy',               'uses' => 'ColorController@destroy']);
});  




