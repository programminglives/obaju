<?php
/**
 * Created by PhpStorm.
 * User: Slayer
 * Date: 5/15/2018
 * Time: 11:28 AM
 */
namespace App;
use DB;



class Helper
{
    static function genderCategorySortPaginate($sort,$genderId,$categoryId){
        switch($sort){
            case 1:
                $products = DB::table('products')
                    ->where('genderId','=',$genderId)->where('categoryId','=',$categoryId)
                    ->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')
                    ->where('genderId','=',$genderId)->where('categoryId','=',$categoryId)
                    ->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')
                    ->where('genderId','=',$genderId)->where('categoryId','=',$categoryId)
                    ->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')
                    ->where('genderId','=',$genderId)->where('categoryId','=',$categoryId)
                    ->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')
                    ->where('genderId','=',$genderId)->where('categoryId','=',$categoryId)
                    ->paginate(12);
        }
        return $products;
    }
}