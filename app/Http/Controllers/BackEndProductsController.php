<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use DB;
use Storage;
use Image;
class BackEndProductsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $products = Product::get();
        // foreach($products as $product)
        // echo $product->categories->categoryName;
        // exit();
      
       

        $data = array(
            'title' => 'Admin Panel||Products',
            'products' => Product::get()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('backend.view_products')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array(
            'title' => 'Admin Panel||Add Products',
            'categories' => DB::table('categories')->get(),
            'brands' => DB::table('brands')->get(),
            'size' => DB::table('sizes')->get(),
            'color' => DB::table('colors')->get()
        );
        return view('backend.add_product')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title="store";
        //validate data
        $this->validate($request,array(
            'productName'  =>  'required|max:150',
            'slug'  =>  'required|alpha_dash|min:5|max:150|unique:products,slug',
            'productNote'  =>  'required',
            'productDescription'  =>  'required',
            'productPrice' => 'required',
            'categoryId'  =>  'required',
            'brandId'  =>  'required',
            'genderId'  =>  'required',
            'color'  =>  'required',
            'size'  =>  'required',
           
        ));
        //store in database
        $product = new Product();
        $product->productName           =   $request->productName;
        $product->slug                  =   $request->slug;
        $product->productNote           =   $request->productNote;
        $product->productDescription    =   $request->productDescription;
        $product->productPrice          =   $request->productPrice;
        $product->categoryId            =   $request->categoryId;
        $product->brandId               =   $request->brandId;
        $product->genderId              =   $request->genderId;
      

        if($request->hasFile('image1')){
          $image1 = $request->file('image1');
          $filename1 = '1.' . time() . '.' . $image1->getClientOriginalExtension();
          $location1 = public_path('images/products/'.$filename1);
          Image::make($image1)->resize(450,600)->save($location1);
          $product->image1               =   $filename1;
        }
        if($request->hasFile('image2')){
            $image2 = $request->file('image2');
            $filename2 = '2.' . time() . '.' . $image2->getClientOriginalExtension();
            $location2 = public_path('images/products/'.$filename2);
            Image::make($image2)->resize(450,600)->save($location2);
            $product->image2               =   $filename2;
        }
        if($request->hasFile('image3')){
            $image3 = $request->file('image3');
            $filename3 = '3.' . time() . '.' . $image3->getClientOriginalExtension();
            $location3 = public_path('images/products/'.$filename3);
            Image::make($image3)->resize(450,600)->save($location3);
            $product->image3               =   $filename3;
        }
        $product->save();
        //get product id of product just saved in database
        $product_id = DB::table('products')->select('productID')->where('slug', $request->slug)->get();

        //insert into product_size table according to size seleted
        for($i = 0; $i < count($request->size); $i++)
        DB::table('product_size')->insert(
            ['size_id' => $request->size[$i], 'product_id' => $product->productId]
        );

        //insert into color_product table according to color seleted
        for($i = 0; $i < count($request->color); $i++)
        DB::table('color_product')->insert(
            ['color_id' => $request->color[$i], 'product_id' => $product->productId]
        );
        //redirect to different page
        $data = array(
            'title' => 'Admin Panel || Product Details',
            'product' => Product::where('slug', '=', $request->slug)->first()
        );
        return view('backend.detail')->with($data);
//        return view('backend.test')->with('title',$title)->with('name',$request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = array(
            'title' => 'Admin Panel || Product Details',
            'product' => Product::where('productId', '=', $id)->first(),
        );
        return view('backend.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = array(
            'title' => 'Admin Panel||Edit Products',
            'product' => DB::table('products')->where('productId', $id)->first(),
            'categories' => DB::table('categories')->get(),
            'brands' => DB::table('brands')->get(),
            'size' => DB::table('sizes')->get(),
            'color' => DB::table('colors')->get()
        );
        return view('backend.edit_product')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate the data
        $product = Product::all()->where('productId',$id)->first();
        if($product->slug == $request->slug){
            $this->validate($request,array(
                'productName'  =>  'required|max:150',
                'productNote'  =>  'required',
                'productDescription'  =>  'required',
                'categoryId'  =>  'required',
                'brandId'  =>  'required',
                'genderId'  =>  'required',
           
                'color'  =>  'required',
                'size'  =>  'required',
                'productPrice'  =>  'required',
            ));
        }else{
            $this->validate($request,array(
                'productName'  =>  'required|max:150',
                'slug'  =>  'required|alpha_dash|min:5|max:150|unique:products,slug',
                'productNote'  =>  'required',
                'productDescription'  =>  'required',
                'categoryId'  =>  'required',
                'brandId'  =>  'required',
                'genderId'  =>  'required',
                'color'  =>  'required',
                'size'  =>  'required',
                'productPrice'  =>  'required',
            ));
        }
        //store in database
        $product = Product::all()->where('productId',$id)->first();                                  //find data first

        if($request->hasFile('image1')){
            $image1 = $request->file('image1');
            $filename1 = '1.' . time() . '.' . $image1->getClientOriginalExtension();
            $location1 = public_path('images/products/'.$filename1);
            Image::make($image1)->resize(450,600)->save($location1);
            $oldFilename                    =   $product->image1;
            $product->image1                =   $filename1;
            Storage::delete($oldFilename);
        }
        if($request->hasFile('image2')){
            $image2 = $request->file('image2');
            $filename2 = '2.' . time() . '.' . $image2->getClientOriginalExtension();
            $location1 = public_path('images/products/'.$filename2);
            Image::make($image2)->resize(450,600)->save($location1);
            $oldFilename                    =   $product->image2;
            $product->image2                =   $filename2;
            Storage::delete($oldFilename);
        }
        if($request->hasFile('image3')){
            $image3 = $request->file('image3');
            $filename3 = '3.' . time() . '.' . $image3->getClientOriginalExtension();
            $location1 = public_path('images/products/'.$filename3);
            Image::make($image3)->resize(450,600)->save($location1);
            $oldFilename                    =   $product->image3;
            $product->image3                =   $filename3;
            Storage::delete($oldFilename);
        }
                                                                                                          //update below
        $product->productName           =   $request->productName;
        $product->slug                  =   $request->slug;
        $product->productNote           =   $request->productNote;
        $product->productDescription    =   $request->productDescription;
        $product->categoryId            =   $request->categoryId;
        $product->brandId               =   $request->brandId;
        $product->genderId              =   $request->genderId;

        $product->productPrice          =   $request->productPrice;

        $product->save();

            //get product id of product just saved in database
            $product = DB::table('products')->select('productID')->where('slug', $request->slug)->get();
            foreach($product as $product)
            $product_id = $product->productID;
            
            DB::table('product_size')->where('product_id', $product_id)->delete();
            DB::table('color_product')->where('product_id', $product_id)->delete();

            //insert into product_size table according to size seleted
            for($i = 0; $i < count($request->size); $i++)
            DB::table('product_size')->insert(
                ['size_id' => $request->size[$i], 'product_id' => $product_id]
            );
    
            //insert into color_product table according to color seleted
            for($i = 0; $i < count($request->color); $i++)
            DB::table('color_product')->insert(
                ['color_id' => $request->color[$i], 'product_id' => $product_id]
            );

        //set flash data with success message
        \Session::flash('success','This product was successfully edited.');
        //redirect with flash data to product/slugname
        //return redirect()->route('backendproducts.show',$oldFilename);

        $data = array(
            'title' => 'Admin Panel || Product Details',
            'product' => Product::where('productId', '=', $id)->first(),
        );
        return view('backend.detail')->with($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::all()->where('productId',$id)->first();
        DB::table('product_size')->where('product_id', $id)->delete();
        DB::table('color_product')->where('product_id', $id)->delete();
        $product->delete();
        return redirect()->route('backendproducts.index');
    }
}
