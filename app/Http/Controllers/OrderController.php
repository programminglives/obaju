<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Model\User;
use App\Model\Cart;
use App\Model\Order;

class OrderController extends Controller
{
    public function order(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'mobile' => 'required|digits:10',
            'city' => 'required',
            'street' => 'required',
            'email' => 'required|max:30',
        ]);


        $user_id = auth()->user()->id;
        $cart = User::find($user_id)->carts;

        foreach($cart as $cart)
        { 
            $exist_order = Order::where('cart_id', $cart->id)->where('user_id', $user_id)->get();
            $cart_id = $cart->id;
            if(count($exist_order) == 0){
            $order = Order::create([
                'name' => $request['name'],
                'mobile' => $request['mobile'],
                'city' => $request['city'],
                'street' => $request['street'],
                'email' => $request['email'],
                'user_id' => $user_id,
                'cart_id' => $cart_id,
            ]);
            Cart::where('id', $cart_id)
            ->update(['status' => 1]);

            }
          

        }
       
   return redirect()->route('home');
    
    }



    public function view()
    {
     $title = "Orders";   
    $order = Order::get();
    // dd($order);
    // foreach($order as $order)
    // $product = $order->carts->products;
    // dd($product);
    return view('backend.order.view')->with('title', $title)->with('order',$order);

    }

    public function destroy(Order $order)
    {
        $order_del = Order::where('id', $order->id)->first();
        if($order_del){
        $order_del->delete();
        }
        
     $title = "Orders";   
    $order = Order::get();
    // dd($order);
    // foreach($order as $order)
    // $product = $order->carts->products;
    // dd($product);
    return view('backend.order.view')->with('title', $title)->with('order',$order);

    }


}
