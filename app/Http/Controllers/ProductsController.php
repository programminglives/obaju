<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!isset($request)) $request->sort = 5;
        switch($request->sort){
            case 1:
                $products = DB::table('products')->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')->paginate(12);
        }
        $data = array(
            'products'  =>  $products,//
            'title'     =>  'Products',
            'count'     =>  DB::table('products')->count()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this -> validate($request, array(
            'productName' => 'required|max:255'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = array(
            'title' => 'Product Details',
            'product' => Product::find($id)
        );
        return view('frontend.detail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
