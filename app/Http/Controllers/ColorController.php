<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Color;

class ColorController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }

    public function view(){
        
        $title = 'Admin Panel || Color';
 
      $color = Color::all();

     //dd($data['size']);
     return view('backend.color.view')->with('title' ,$title)->with('color',$color);
 }

 public function create(){
     $data = array(
         'title' => 'Admin Panel || Color'
     );
     return view('backend.color.create')->with($data);
 }

 public function store(Request $request){
     $request->validate([
         'name' => 'required|string|max:255'
     ]);

 Color::create([
     'name' => $request['name'],
 ]);
 
 $title = 'Admin Panel || Color';
 
 $color = Color::all();

//dd($data['size']);
return view('backend.color.view')->with('title' ,$title)->with('color',$color);
 }

 public function destroy(Color $color)
 {
     $color_del = Color::where('id', $color->id)->first();
     if($color_del){
     $color_del->delete();
     }
     return redirect()->route('backend.color.view');
 }
}
