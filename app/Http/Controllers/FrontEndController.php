<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Helper;
use DB;
use Auth;
use App\Model\Cart;

class FrontEndController extends Controller
{
    public function index(){
        $title = "Online Shop";
        return view('frontend.index',compact('title'));
    }
    public function products(){
        $title = "Browse Products";
        return view('frontend.products',compact('title'));
    }
    public function contact(){
        $title = "Contact Us!!";
        $data = array(
            'title' => 'Contact',
            'contact' => ['Online Clothing Store','Cash On Delivery']
        );
        return view('frontend.contact')->with($data);
    }
    
    public function customerAccount(){
        $title = "Customer Account";
        return view('frontend.customer.customer-account')->with('title',$title);
    }
    public function customerOrder(){
        $title = "Order Details";
        return view('frontend.customer.customer-order')->with('title',$title);
    }
    public function customerOrders(){
        $title = "Orders";
        return view('frontend.customer.customer-orders')->with('title',$title);
    }
    public function customerWishlist(){
        $title = "Wishlist";
        return view('frontend.customer.customer-wishlist')->with('title',$title);
    }
   
    public function checkout1(){
        $user_id = auth()->user()->id;
        //dd($user_id);
        $cart = Cart::where('user_id',$user_id)->where('status', 0)->get();
        $totalprice =Cart::where('user_id',$user_id)->where('status',0)->sum('price');
        //dd($totalprice);
        $title = "Checkout";
        return view('frontend.order.checkout1')->with('title',$title)->with('cart',$cart)->with('totalprice',$totalprice);
    }
    public function checkout2(){
        $title = "Checkout Step 2";
        return view('frontend.order.checkout2')->with('title',$title);
    }
    public function checkout3(){
        $title = "Checkout Step 3";
        return view('frontend.order.checkout3')->with('title',$title);
    }
    public function checkout4(){
        $title = "Checkout Step 4";
        return view('frontend.order.checkout4')->with('title',$title);
    }
    public function getSingle($slug){

        
        $data = array(
            'title' => 'Product Details',
            'product' => Product::where('slug', '=', $slug)->first()
        );
        return view('frontend.detail')->with($data);
    }
    //---------------------============Header Controller Men Clothing============-----------------------------//
    public function menShirt(Request $request){
        switch($request->sort){
            case 1:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',1)
                    ->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',1)
                    ->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',1)
                    ->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',1)
                    ->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',1)
                    ->paginate(12);
        }
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s T-Shirt',
            'count'     =>  $products->total()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menTshirt(Request $request){
        switch($request->sort){
            case 1:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',2)
                    ->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',2)
                    ->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',2)
                    ->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',2)
                    ->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',2)
                    ->paginate(12);
        }
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s T-Shirt',
            'count'     =>  $products->total()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menPant(Request $request){
        switch($request->sort){
            case 1:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',3)
                    ->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',3)
                    ->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',3)
                    ->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',3)
                    ->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')
                    ->where('genderId','=',1)->where('categoryId','=',3)
                    ->paginate(12);
        }
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Pant',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menCoat(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,4);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menWindCheater(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,5);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Wind Cheater',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menJacket(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,6);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menHoodies(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,7);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menShorts(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,8);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menSocks(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,9);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //---------------------==============Header Controller Men Shoes=============-----------------------------//
    public function menSportShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,10);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Sport Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menCasualShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,11);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Casual Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menFormalShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,12);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Formal Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menBoots(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,13);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Boots',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menSandalsAndSlippers(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,14);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Sandals and Slippers',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //------------------==============Header Controller Men Accessories=============--------------------------//
    public function menBags(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,15);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Bags',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menCapHatGloves(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,16);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Cap, Hats, Gloves',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function menGoggles(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,1,17);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Men\'s Goggles',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //---------------------===========Header Controller Women Clothing===========-----------------------------//
    public function womenTshirt(Request $request){
        switch($request->sort){
            case 1:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',2)
                    ->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',2)
                    ->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',2)
                    ->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',2)
                    ->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',2)
                    ->paginate(12);
        }
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s T-Shirt',
            'count'     =>  $products->total()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenShirt(Request $request){
        switch($request->sort){
            case 1:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',1)
                    ->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',1)
                    ->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',1)
                    ->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',1)
                    ->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',1)
                    ->paginate(12);
        }
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s T-Shirt',
            'count'     =>  $products->total()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenPant(Request $request){
        switch($request->sort){
            case 1:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',3)
                    ->orderBy('productPrice','asc')->paginate(12);
                break;
            case 2:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',3)
                    ->orderBy('productPrice','desc')->paginate(12);
                break;
            case 3:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',3)
                    ->orderBy('created_at','asc')->paginate(12);
                break;
            case 4:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',3)
                    ->orderBy('created_at','desc')->paginate(12);
                break;
            default:
                $products = DB::table('products')
                    ->where('genderId','=',2)->where('categoryId','=',3)
                    ->paginate(12);
        }
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Pant',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenWindCheater(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,5);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Wind Cheater',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenCoat(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,4);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenJacket(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,6);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenHoodies(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,7);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenShorts(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,8);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenSocks(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,9);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //---------------------=============Header Controller Women Shoes============-----------------------------//
    public function womenBoots(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,13);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Boots',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenSportShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,10);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Sport Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenCasualShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,11);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Casual Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenHikingShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,18);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Hiking Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenFormalShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,12);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Formal Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenHeels(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,19);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Heels',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenSandalsAndSlippers(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,14);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Sandals and Slippers',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //------------------=============Header Controller Women Accessories============--------------------------//
    public function womenBags(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,15);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Bags',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenCapHatGloves(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,16);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Cap, Hats, Gloves',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function womenGoggles(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,2,17);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Women\'s Goggles',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //---------------------===========Header Controller Kids Clothing===========-----------------------------//
    public function kidsTshirt(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,2);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s T-Shirt',
            'count'     =>  $products->total()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsShirt(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,1);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s T-Shirt',
            'count'     =>  $products->total()//Product::all(),
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsPant(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,3);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Pant',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsJacket(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,6);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsShorts(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,8);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsSocks(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,9);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Coat',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //---------------------=============Header Controller Kids Shoes============-----------------------------//
    public function kidsBoots(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,13);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Boots',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsSportShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,10);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Sport Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsSchoolShoes(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,20);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Casual Shoes',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsSandalsAndSlippers(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,14);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Sandals and Slippers',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    //------------------=============Header Controller Kids Accessories============--------------------------//
    public function kidsBags(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,15);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Bags',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsCapHatGloves(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,16);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Cap, Hats, Gloves',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
    public function kidsGoggles(Request $request){
        $products = Helper::genderCategorySortPaginate($request->sort,4,17);
        $data = array(
            'products'  =>  $products,
            'title'     =>  'Kid\'s Goggles',
            'count'     =>  $products->total(),//Product::all(),
            'sort'      =>  '1'
//            'productsOrderAsc' => Product::orderBy('productName','asc')->get(),
//            'productsOrderDesc' => Product::orderBy('productName','desc')->take(1)->get(),
//            'productByName' => Product::where('productName','pant')->get()->paginate(1)
        );
        return view('frontend.products')->with($data);
    }
}
