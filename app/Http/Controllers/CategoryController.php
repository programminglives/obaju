<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Category;

class CategoryController extends Controller
{
    public function create(){
        $data = array(
            'title' => 'Admin Panel || Category'
        );
        return view('backend.category.create')->with($data);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        Category::create([
        'categoryName' => $request['name'],
    ]);
    
    $title = 'Admin Panel || Category';
    
    $category = Category::all();

   //dd($data['size']);
   return view('backend.category.view')->with('title' ,$title)->with('category',$category);
    }

    public function view(){
        
        $title = 'Admin Panel || Category';
 
      $category = Category::all();
      //dd($brand);

     //dd($data['size']);
     return view('backend.category.view')->with('title' ,$title)->with('category',$category);
 }

 public function destroy(Category $category)
    {
        $category_del = Category::where('categoryId', $category->categoryId)->first();
        if($category_del){
        $category_del->delete();
        }
        return redirect()->route('backend.category.view');
    }
}
