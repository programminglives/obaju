<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Admin;
use Auth;

class BackendAdminController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth:admin')->except('login','login_form','register','store');
    }
    
    public function register(){
        $title = "Login || Register";
        return view('backend.auth.register',compact('title'));
    }

    public function view(){
        $title = "Admin";
        $admin = Admin::all();
        return view('backend.auth.view')->with('title' ,$title)->with('admin',$admin);
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:admins|max:30',
            'email' => 'required|unique:admins|max:30',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]);

        $admin = Admin::create([
            'name' => $request['name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password'=> bcrypt($request['password']),
        ]);

        Auth::guard('admin')->login($admin);
   
        return redirect()->route('dashboard');
       
    }
    
    public function login_form(){
        $title = "Log In";
     return view('backend.auth.login')->with('title',$title);
    }

    public function login(Request $request){
        $this->validate($request,[
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($request->has('back')){
            if(Auth::guard('admin')->attempt(['username' => $request->username, 'password'=> $request->password])){

                return redirect()->to($request->get('back'));
            }
        }

        elseif(Auth::guard('admin')->attempt(['username' => $request->username, 'password'=> $request->password])){
     return redirect()->intended(route('dashboard'));
    }

    return redirect()->back();
    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('dashboard');
    }

    public function destroy(Admin $admin)
    {
        $admin_del = Admin::where('id', $admin->id)->first();
        $username = $admin_del->username;
        if($admin_del){
            if($username != 'superadmin')
        $admin_del->delete();
        }
        return redirect()->route('backend.admin.view');
    }
    
}
