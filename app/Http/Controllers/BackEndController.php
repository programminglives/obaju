<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class BackEndController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $data = array(
            'title' => 'Admin Panel || Dashboard',
            'product' => Product::all()->first()
        );
        return view('backend.dashboard')->with($data);
    }
    public function getSingle($slug){
       
        $data = array(
            'title' => 'Admin Panel || Product Details',
            'product' => Product::where('slug', '=', $slug)->first()
           
        );
        return view('backend.detail')->with($data);
    }
}
