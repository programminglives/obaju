<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Product;
use App\Model\Cart;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
     
    public function __construct()
    {
        $this->middleware('auth');
    }
   


    public function store(Request $request){
      

        
           $product_id = $request->product_id;
           $user_id = auth()->user()->id;
           $size = $request->size;
           $color = $request->color;
           $product = Product::where('productId' , '=' , $product_id)->first();
           $price = $product->productPrice;
 

           $exist_cart = Cart::where([
            ['user_id', '=' ,$user_id],
            ['product_id', '=', $product_id],
            ['status', '=', 0]
        ])->first();

        if (count($exist_cart) == 0) {
            Cart::create([
                'product_id' => $product_id,
                'size_id'=> $size,
                'color_id'=> $color,
                
                'user_id' => $user_id,
                'price' => $price,
            ]);
        }
    
           $title = "Cart";
           $cart = Cart::where('user_id',$user_id)->where('status',0)->get();
           $totalprice = Cart::where('user_id',$user_id)->where('status',0)->sum('price');
           
            return view('frontend.order.cart')->with('title',$title)->with('cart',$cart)->with('totalprice',$totalprice);
       
    }

    public function view(){

      
            $user_id = auth()->user()->id;
       
            $title = "Cart";
            $cart = Cart::where('user_id',$user_id)->where('status',0)->get();
            $totalprice = Cart::where('user_id',$user_id)->where('status',0)->sum('price');

        
        
            return view('frontend.order.cart')->with('title',$title)->with('cart',$cart)->with('totalprice',$totalprice);
    }

    public function destroy(Cart $cart)
    {
       
        $cart_del = Cart::where('id', $cart->id)->first();
        if($cart_del){
        $cart_del->delete();
        }
        return redirect()->route('frontend.cart.view');
    }
    
    public function changeQuantity(Request $request)
    {
       
        if(request()->ajax()){
            $cart_id = $request->cart_id;
            $quantity = $request->quantity;

            $cart = Cart::find($cart_id);
            $price = $cart->products->productPrice;
            $cart->quantity = $quantity;
            $cart->price = $quantity * $price;
            $save = $cart->save();
            $totalprice = Cart::where('status',0)->sum('price');
           
            return response()->json(['success'=>true,'totalprice' => $totalprice]);
        }
    }
    }