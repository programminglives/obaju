<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use Auth;



class FrontendUserController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('guest')->except('logout','update');
    }
    

    public function register(){
        $title = "Login || Register";
        return view('frontend.register',compact('title'));
    }

    public function store(Request $request){

        $request->validate([
            'name' => 'required',
            'mobile' => 'required|unique:users|digits:10',
            'address' => 'required',
            'gender' => 'required',
            'username' => 'required|unique:users|max:30',
            'email' => 'required|unique:users|max:30',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]);

        $user = User::create([
            'name' => $request['name'],
            'mobile' => $request['mobile'],
            'address' => $request['address'],
            'gender' => $request['gender'],
            'username' => $request['username'],
            'email' => $request['email'],
            'password'=> bcrypt($request['password']),
        ]);

        Auth::login($user);
   
        return redirect()->route('home');
       
    }

    public function update(Request $request, User $user)
    {
        

        $request->validate([
            'name' => 'required',
            'mobile' => 'required|digits:10',
            'address' => 'required',
            'email' => 'required|max:30',
       ]);


        $user = User::find($user)->first();
        $user->name = $request->input('name');
        $user->mobile = $request->input('mobile');
        $user->address = $request->input('address');
        $user->email = $request->input('email');
        $user->save();
        
       return redirect()->route('frontend.user.checkout3');
    }


    public function login(Request $request){
        $this->validate($request,[
            'username' => 'required',
            'password' => 'required'
        ]);

       
            if(Auth::guard()->attempt(['username' => $request->username, 'password'=> $request->password])){
                return redirect()->back();
            }
        
    return redirect()->back();
    }

    public function logout()
    {
        Auth::guard()->logout();
        return redirect()->back();
    }

    
}
