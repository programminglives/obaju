<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Size;

class SizeController extends Controller
{

    // public function __construct()
    // {
    //     $this->middleware('auth:admin');
    // }
    public function view(){
        
           $title = 'Admin Panel || Size';
    
         $size = Size::all();

        //dd($data['size']);
        return view('backend.size.view')->with('title' ,$title)->with('size',$size);
    }

    public function create(){
        $data = array(
            'title' => 'Admin Panel || Size'
        );
        return view('backend.size.create')->with($data);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

    Size::create([
        'name' => $request['name'],
    ]);
    
    $title = 'Admin Panel || Size';
    
    $size = Size::all();

   //dd($data['size']);
   return view('backend.size.view')->with('title' ,$title)->with('size',$size);
    }

    public function destroy(Size $size)
    {
        $size_del = Size::where('id', $size->id)->first();
        if($size_del){
        $size_del->delete();
        }
        return redirect()->route('backend.size.view');
    }
    
}
