<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Model\Manufacturer;

class BrandController extends Controller
{
    public function create(){
        $data = array(
            'title' => 'Admin Panel || Brand'
        );
        return view('backend.brand.create')->with($data);
    }

    public function store(Request $request){
        $request->validate([
            'name' => 'required|string|max:255'
        ]);

        Manufacturer::create([
        'brandName' => $request['name'],
    ]);
    
    $title = 'Admin Panel || Brand';
    
    $manufacturer = Manufacturer::all();

   //dd($data['size']);
   return view('backend.brand.view')->with('title' ,$title)->with('manufacturer',$manufacturer);
    }

    public function view(){
        
        $title = 'Admin Panel || Brand';
 
      $manufacturer = Manufacturer::all();
      //dd($brand);

     //dd($data['size']);
     return view('backend.brand.view')->with('title' ,$title)->with('manufacturer',$manufacturer);
 }

 public function destroy(Manufacturer $manufacturer)
    {
        $brand_del = Manufacturer::where('brandId', $manufacturer->brandId)->first();
        if($brand_del){
        $brand_del->delete();
        }
        return redirect()->route('backend.brand.view');
    }
}
