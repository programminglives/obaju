<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Product extends Model
{
    //Table Name
    protected $table = 'products';
    //Primary Key Field
    public $primaryKey = 'productId';
    //Timestamps
    public $timestamps = true;

    //create many to many relation with color
    public function colors()
    {
        return $this->belongsToMany('App\Model\Color', 'color_product', 'product_id', 'color_id');
    }
   
    //create many to many relation with size
    public function sizes()
    {
        return $this->belongsToMany('App\Model\Size', 'product_size', 'product_id', 'size_id');
    }

    public function manufacturers()
    {
        return $this->belongsTo('App\Model\Manufacturer', 'brandId');
    }

    public function categories()
    {
        return $this->belongsTo('App\Model\Category', 'categoryId');
    }

}
