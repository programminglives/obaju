<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{

    protected $table='sizes';
    protected $fillable = [
        'name', 
    ];

    //create many to many relation with product
    public function products()
    {
        return $this->belongsToMany('App\Model\Product');
    }

}
