<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    protected $fillable = [
        'product_id','user_id','size_id','color_id','quantity','price'
    ];

    public function users()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function products()
    {
        return $this->belongsTo('App\Product','product_id');
    }

    public function colors()
    {
        return $this->belongsTo('App\Model\Color','color_id');
    }


    public function sizes()
    {
        return $this->belongsTo('App\Model\Size','size_id');
    }


    
}
