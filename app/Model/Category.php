<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = [
        'categoryName',
    ];
    //Table Name
    protected $table = 'categories';
    //Primary Key Field
    public $primaryKey = 'categoryId';
    //Timestamps
    public $timestamps = true;

    public function products()
    {
        return $this->hasMany('App\Model\Product');
    }
}
