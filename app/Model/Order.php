<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id','cart_id','name','city','street','mobile','email'
    ];


    public function users()
    {
        return $this->belongsTo('App\Model\User', 'user_id');
    }

    public function carts()
    {
        return $this->belongsTo('App\Model\Cart','cart_id');
    }
}
