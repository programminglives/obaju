<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{

    protected $fillable = [
        'brandName',
    ];
    //Table Name
    protected $table = 'brands';
    //Primary Key Field
    public $primaryKey = 'brandId';
    //Timestamps
    public $timestamps = true;


    public function products()
    {
        return $this->hasMany('App\Model\Product');
    }
}
