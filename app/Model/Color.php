<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table='colors';
    protected $fillable = [
        'name', 
    ];

    //create many to many realation with product
    public function products()
    {
        return $this->belongsToMany('App\Model\Product');
    }
}
