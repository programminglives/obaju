@extends('layouts.backapp')

@section('content')
    <h3 class="header smaller lighter blue">Add Brand Below:</h3>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="form-horizontal" role="form" action="{{route("backend.brand.store")}}" method="post" id="add-product">
        {{ csrf_field() }}

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Brand Name </label>
            <div class="col-sm-9">
                <input name="name" value="" type="text" id="form-field-1" placeholder="Brand Name" class="col-xs-10 col-sm-5" />
            </div>
        </div>

         <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> </label>
            <div class="col-sm-9">
                <button type="submit" form="add-product" value="submit" class="btn btn-white btn-info btn-bold">
                    <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                    Add Brand
                </button>
            </div>
        </div>

    </form>
    <script src="{{ asset('tinymce/tinymce.min.js') }}"></script>
    <script>
        var editor_config = {
            path_absolute : "{{ URL::to('/') }}/",
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }
                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };
        tinymce.init(editor_config);


    </script>
@endsection
