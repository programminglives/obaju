@extends('layouts.backapp')

@section('content')
    <h3 class="header smaller lighter blue">Add Product Details Below:</h3>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="form-horizontal" role="form" enctype="multipart/form-data" action="/backendproducts" method="post" id="add-product">
        {{ csrf_field() }}

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Name </label>
            <div class="col-sm-9">
                <input name="productName" value="" type="text" id="form-field-1" placeholder="Product Name" class="col-xs-10 col-sm-5" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product URL </label>
            <div class="col-sm-9">
                <input name="slug" type="text" id="form-field-1" placeholder="Product URL" class="col-xs-10 col-sm-5" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Note </label>
            <div class="col-sm-9">
                <input name="productNote" type="text" id="form-field-1-1" placeholder="Short Note about the Product" class="form-control" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Description </label>
            <div class="col-sm-9">
                <textarea name="productDescription">Enter Product's Description Here!!!</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category </label>
            <div class="col-sm-9">
                <select name="categoryId" class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose a State...">
                    <option value="">Click here to select</option>
                    @if(count($categories) > 0)
                        @foreach($categories as $category)
                            <option value="{{ $category->categoryId }}">{{ $category->categoryName }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Brand </label>
            <div class="col-sm-9">
                <select name="brandId" class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose a State...">
                    <option value=""> Click here to select </option>
                    @if(count($brands) > 0)
                        @foreach($brands as $brand)
                            <option value="{{ $brand->brandId }}">{{ $brand->brandName }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type </label>
            <div class="col-sm-9">
                <select name="genderId" class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose a State...">
                    <option value=""> Click here to select </option>
                    <option value="1">Male</option>
                    <option value="0">Female</option>
                    <option value="2">Unisex</option>
                    <option value="3">Kids</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Available Sizes </label>
            <div class="col-sm-9">
            @foreach($size as $size)
            <div class="checkbox">
				<label>
				<input name="size[]" value="{{$size->id}}" type="checkbox" class="ace" />
				<span class="lbl"> {{$size->name}}</span>
					</label>
				</div>
            @endforeach

            </div>

        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Colors </label>
            <div class="col-sm-9">
            @foreach($color as $color)
            <div class="checkbox">
				<label>
				<input name="color[]" value="{{$color->id}}" type="checkbox" class="ace" />
				<span class="lbl"> {{$color->name}}</span>
					</label>
				</div>
            @endforeach
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Price </label>
            <div class="col-sm-9">
                <input name="productPrice" type="text" id="form-field-1" placeholder="Rs. 3450" class="col-xs-10 col-sm-5" />
            </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label no-padding-right">Main Image Input</label>
          <div class="col-md-9">
            <input type="file"  name="image1" class="btn btn-default" id="fileToUpload">
            <p class="help-block">
              This is the face of the product [450x678].
            </p>
          </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label no-padding-right">Secondary Image Input</label>
            <div class="col-md-9">
                <input type="file"  name="image2" class="btn btn-default" id="fileToUpload">
                <p class="help-block">
                    This is an image of the product [450x678].
                </p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label no-padding-right">Secondary Image Input</label>
            <div class="col-md-9">
                <input type="file"  name="image3" class="btn btn-default" id="fileToUpload">
                <p class="help-block">
                    This is an image of the product [450x678].
                </p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> </label>
            <div class="col-sm-9">
                <button type="submit" form="add-product" value="submit" class="btn btn-white btn-info btn-bold">
                    <i class="ace-icon fa fa-floppy-o bigger-120 blue"></i>
                    Add Product
                </button>
            </div>
        </div>
    </form>
    <script src="{{ asset('tinymce/tinymce.min.js') }}"></script>
    <script>
        var editor_config = {
            path_absolute : "{{ URL::to('/') }}/",
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }
                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };
        tinymce.init(editor_config);


    </script>
@endsection
