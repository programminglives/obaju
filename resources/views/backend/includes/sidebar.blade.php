<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
    <script type="text/javascript">
        try{ace.settings.loadState('sidebar')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->

    <ul class="nav nav-list">
        <li class="active">
            <a href="/dashboard">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>

         <li class="">
            <a href="" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-bag"></i>
                <span class="menu-text">
								Order
							</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
              
                <li class="">
                    <a href="{{ route('backend.order.view') }}">
                        <i class="menu-icon fa fa-eye pink"></i>
                        View Order
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-bag"></i>
                <span class="menu-text">
								Brand
							</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="{{ route('backend.brand.create') }}">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Add Brand
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="{{ route('backend.brand.view') }}">
                        <i class="menu-icon fa fa-eye pink"></i>
                        View Brand
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        <li class="">
            <a href="" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-bag"></i>
                <span class="menu-text">
								Categories
							</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="{{ route('backend.category.create') }}">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Add category
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="{{ route('backend.category.view') }}">
                        <i class="menu-icon fa fa-eye pink"></i>
                        View Category
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-bag"></i>
                <span class="menu-text">
								Products
							</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="{{ url('backendproducts/create') }}">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Add Product
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="{{ url('backendproducts') }}">
                        <i class="menu-icon fa fa-eye pink"></i>
                        View Products
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

          <li class="">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-bag"></i>
                <span class="menu-text">
								Size
							</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="{{ route('backend.size.create') }}">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Add Size
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="{{ route('backend.size.view') }}">
                        <i class="menu-icon fa fa-eye pink"></i>
                        View Size
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

          <li class="">
            <a href="" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-bag"></i>
                <span class="menu-text">
								Color
							</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

            <ul class="submenu">
                <li class="">
                    <a href="{{ route('backend.color.create') }}">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Add Color
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="{{ route('backend.color.view') }}">
                        <i class="menu-icon fa fa-eye pink"></i>
                        View Color
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

        <li class="">
            <a href="" class="dropdown-toggle">
                <i class="menu-icon fa fa-shopping-bag"></i>
                <span class="menu-text">
								Admin
							</span>

                <b class="arrow fa fa-angle-down"></b>
            </a>

            <b class="arrow"></b>

        <ul class="submenu">
                <li class="">
                    <a href="{{ route('backend.admin.register') }}">
                        <i class="menu-icon fa fa-plus purple"></i>
                        Add Admin
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="{{ route('backend.admin.view') }}">
                        <i class="menu-icon fa fa-eye pink"></i>
                        View Admin
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul>
        </li>

      
    </ul><!-- /.nav-list -->

    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>