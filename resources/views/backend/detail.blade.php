@extends('layouts.backapp')

    @section('content')

        <h2 class="header smaller lighter blue">{{ $product->productName }}</h2>
        <div class="row">
            <div class="col-md-6">
                <h3>Product Description</h3>
                <p> {!! $product->productDescription !!} </p>
                <h3>Product Note</h3>
                <p> {{ $product->productNote }} </p>
                <h3>Product Category</h3>
                <p> {{ $product->categoryId }} </p>
                <h3>Product Brand</h3>
                <p> {{ $product->brandId }} </p>
                <h3>Product Gender</h3>
                <p> {{ $product->genderId }} </p>
            </div>
            <div class="col-md-6">
            
                <h3>Product Price</h3>
                <p> {{ $product->productPrice }} </p>
                <h3>Product Size</h3>
                @foreach($product->sizes as $size)
                <p> {{ $size->name }} </p>
                @endforeach
                <h3>Product Color</h3>
                @foreach($product->colors as $color)
                <p> {{ $color->name }} </p>
                @endforeach
                <h3>Product Created Date</h3>
                <p> {{ $product->created_at }} </p>
                <h3>Product Updated Date</h3>
                <p> {{ $product->updated_at }} </p>
            </div>
        </div>
    @endsection