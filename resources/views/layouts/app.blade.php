<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta name="robots" content="all,follow">
        <meta name="googlebot" content="index,follow,snippet,archive">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Obaju e-commerce template">
        <meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
        <meta name="keywords" content="">

        <title>
            {{$title}}
        </title>

        <meta name="keywords" content="">

        <link rel="stylesheet" href="{{asset('css/app.css')}}">

        <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

        <!-- styles -->
        <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
        <link href="{{asset('css/owl.theme.css')}}" rel="stylesheet">

        <!-- theme stylesheet -->
        <link href="{{asset('css/style.default.css')}}" rel="stylesheet" id="theme-stylesheet">

        <!-- your stylesheet with modifications -->
        <link href="{{asset('css/custom.css')}}" rel="stylesheet">

        <script src="{{asset('js/respond.min.js')}}"></script>

        <link rel="shortcut icon" href="{{asset('favicon.png')}}">

<script>
	 //jquery for add to cart
         function quantityChange(cart_id){
			 //alert(pid);
             var cart_id = cart_id;
             var quantity = document.getElementById("quantity" + cart_id).value;
            
             
            $.ajax({
               type:'POST',
			   url:'/cart/change/quantity',
			   data:{ cart_id:cart_id,quantity:quantity, _token: '{!! csrf_token() !!}' },
			   success: function (data) {
				   if(data["success"] == true){
                    document.getElementById('totalprice').innerHTML = 'NRS' +data["totalprice"];
                    $("#quantityprice" + cart_id).load(" #quantityprice" + cart_id);
                   }
               }
		});
	}

       </script> 


    </head>

    <body>

        @include('frontend.includes.header')

        <div id="all">

        @yield('content')

        @include('frontend.includes.footer')



        </div>
        <!-- /#all -->




        <!-- *** SCRIPTS TO INCLUDE ***
        _________________________________________________________ -->
        <script src="{{asset('js/jquery-1.11.0.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/jquery.cookie.js')}}"></script>
        <script src="{{asset('js/waypoints.min.js')}}"></script>
        <script src="{{asset('js/modernizr.js')}}"></script>
        <script src="{{asset('js/bootstrap-hover-dropdown.js')}}"></script>
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('js/front.js')}}"></script>

        @yield('contactmap')

    </body>

</html>