<!-- *** TOPBAR ***
    _________________________________________________________ -->
<div id="top">
    <div class="container">
        <div class="col-md-6 offer" data-animate="fadeInDown">
            <a href="#" class="btn btn-success btn-sm" data-animate-hover="shake">Offer of the day</a>  <a href="#">Get flat 35% off on orders over $50!</a>
        </div>
        @guest
        <div class="col-md-6" data-animate="fadeInDown">
            <ul class="menu">
                <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                </li>
                <li><a href="{{route('frontend.user.register')}}">Register</a>
                </li>
                <li><a href="/contact">Contact</a>
                </li>
                <li><a href="#">Recently viewed</a>
                </li>
            </ul>
        </div>
        @else
        <div class="col-md-6" data-animate="fadeInDown">
            <ul class="menu">
              
                <li><a href="">{{ucwords(Auth::user()->name)}}</a>
                </li>
                
                <li><a href="{{route('frontend.user.logout')}}">Log Out</a>
                </li>
                <li><a href="/contact">Contact</a>
                </li>
            </ul>
        </div>
        @endguest
    </div>
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
   
        <div class="modal-dialog modal-sm">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="Login">Customer login</h4>
                </div>
                <div class="modal-body">
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                    <form action="{{route('frontend.user.login')}}" method="post">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" id="email-modal" placeholder="username" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" id="password-modal" placeholder="password" required>
                        </div>

                        <p class="text-center">
                            <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                        </p>

                    </form>

                    <p class="text-center text-muted">Not registered yet?</p>
                    <p class="text-center text-muted"><a href="/register"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to special discounts and much more!</p>

                </div>
            </div>
        </div>
    </div>

</div>

<!-- *** TOP BAR END *** -->

<!-- *** NAVBAR ***
_________________________________________________________ -->

<div class="navbar navbar-default yamm" role="navigation" id="navbar">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand home" href="/" data-animate-hover="bounce">
                <img src="img/logo.png" alt="Obaju logo" class="hidden-xs">
                <img src="img/logo-small.png" alt="Obaju logo" class="visible-xs"><span class="sr-only">Obaju - go to homepage</span>
            </a>
            <div class="navbar-buttons">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-align-justify"></i>
                </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                    <span class="sr-only">Toggle search</span>
                    <i class="fa fa-search"></i>
                </button>
                <a class="btn btn-default navbar-toggle" href="/cart/view">
                    <i class="fa fa-shopping-cart"></i>  <span class="hidden-xs">3 items in cart</span>
                </a>
            </div>
        </div>
        <!--/.navbar-header -->

        <div class="navbar-collapse collapse" id="navigation">

            <ul class="nav navbar-nav navbar-left">
                <li class="active"><a href="/">Home</a>
                </li>

                <li class="dropdown yamm-fw">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Men <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h5>Clothing</h5>
                                        <ul>
                                            <li><a href="/mens-tshirt">T-shirts</a></li>
                                            <li><a href="/mens-shirt">Shirts</a></li>
                                            <li><a href="/mens-pant">Pants</a></li>
                                            <li><a href="/mens-windcheater">Wind Cheater</a></li>
                                            <li><a href="/mens-jackets">Jackets</a></li>
                                            <li><a href="/mens-hoodies">Hoodies</a></li>
                                            <li><a href="/mens-shorts">Shorts</a></li>
                                            <li><a href="/mens-socks">Socks</a></li>
                                            <li><a href="/products">All Products</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Shoes</h5>
                                        <ul>
                                            <li><a href="/mens-sport-shoes">Sport Shoes</a></li>
                                            <li><a href="/mens-casual-shoes">Casual Shoes</a></li>
                                            <li><a href="/mens-formal-shoes">Formal Shoes</a></li>
                                            <li><a href="/mens-boots">Boots</a></li>
                                            <li><a href="/mens-sandals-slippers">Sandals and Slippers</a></li>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Accessories</h5>
                                        <ul>
                                            <li><a href="/mens-bags">Bags</a></li>
                                            <li><a href="/mens-caps-hats-gloves">Caps Hats Gloves</a></li>
                                            <li><a href="/mens-goggles">Goggles</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Featured</h5>
                                        <ul>
                                            <li><a href="/products">Trainers</a>
                                            </li>
                                            <li><a href="/products">Sandals</a>
                                            </li>
                                            <li><a href="/products">Hiking shoes</a>
                                            </li>
                                        </ul>
                                        <h5>Looks and trends</h5>
                                        <ul>
                                            <li><a href="/products">Trainers</a>
                                            </li>
                                            <li><a href="/products">Sandals</a>
                                            </li>
                                            <li><a href="/products">Hiking shoes</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.yamm-content -->
                        </li>
                    </ul>
                </li>

                <li class="dropdown yamm-fw">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Women <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h5>Clothing</h5>
                                        <ul>
                                            <li><a href="/womens-tshirt">T-Shirts</a></li>
                                            <li><a href="/womens-shirt">Shirts</a></li>
                                            <li><a href="/womens-pant">Pants</a></li>
                                            <li><a href="/womens-windcheater">Wind Cheater</a></li>
                                            <li><a href="/womens-coat">Coat</a></li>
                                            <li><a href="/womens-jackets">Jackets</a></li>
                                            <li><a href="/womens-hoodies">Hoodies</a></li>
                                            <li><a href="/womens-shorts">Shorts</a></li>
                                            <li><a href="/womens-socks">Socks</a></li>
                                            <li><a href="/products">All Products</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Shoes</h5>
                                        <ul>
                                            <li><a href="/womens-boots">Boots</a></li>
                                            <li><a href="/womens-sport-shoes">Sport Shoes</a></li>
                                            <li><a href="/womens-casual-shoes">Casual Shoes</a></li>
                                            <li><a href="/womens-hiking-shoes">Hiking Shoes</a></li>
                                            <li><a href="/womens-formal-shoes">Formal Shoes</a></li>
                                            <li><a href="/womens-heels">Heels</a></li>
                                            <li><a href="/womens-flats-sandals">Flats & Sandals</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Accessories</h5>
                                        <ul>
                                            <li><a href="/womens-bags">Bags</a></li>
                                            <li><a href="/womens-caps-hats-gloves">Caps Hats Gloves</a></li>
                                            <li><a href="/womens-goggles">Goggles</a></li>
                                        </ul>
                                        <h5>Looks and trends</h5>
                                        <ul>
                                            <li><a href="/products">Trainers</a>
                                            </li>
                                            <li><a href="/products">Sandals</a>
                                            </li>
                                            <li><a href="/products">Hiking shoes</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="banner">
                                            <a href="#">
                                                <img src="img/banner.jpg" class="img img-responsive" alt="">
                                            </a>
                                        </div>
                                        <div class="banner">
                                            <a href="#">
                                                <img src="img/banner2.jpg" class="img img-responsive" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.yamm-content -->
                        </li>
                    </ul>
                </li>

                <li class="dropdown yamm-fw">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Kids <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h5>Clothing</h5>
                                        <ul>
                                            <li><a href="/kids-tshirt">T-shirts</a></li>
                                            <li><a href="/kids-shirt">Shirts</a></li>
                                            <li><a href="/kids-pant">Pants</a></li>
                                            <li><a href="/kids-jackets">Jackets</a></li>
                                            <li><a href="/kids-shorts">Shorts</a></li>
                                            <li><a href="/kids-socks">Socks</a></li>
                                            <li><a href="/products">All Products</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Shoes</h5>
                                        <ul>
                                            <li><a href="/kids-boots">Boots</a></li>
                                            <li><a href="/kids-sport-shoes">Sport Shoes</a></li>
                                            <li><a href="/kids-school-shoes">School Shoes</a></li>
                                            <li><a href="/kids-flats-sandals">Sandals</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Accessories</h5>
                                        <ul>
                                            <li><a href="/kids-bags">Bags</a></li>
                                            <li><a href="/kids-caps-hats-gloves">Caps Hats Gloves</a></li>
                                            <li><a href="/kids-goggles">Goggles</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Featured</h5>
                                        <ul>
                                            <li><a href="/products">Trainers</a>
                                            </li>
                                            <li><a href="/products">Sandals</a>
                                            </li>
                                            <li><a href="/products">Hiking shoes</a>
                                            </li>
                                        </ul>
                                        <h5>Looks and trends</h5>
                                        <ul>
                                            <li><a href="/products">Trainers</a>
                                            </li>
                                            <li><a href="/products">Sandals</a>
                                            </li>
                                            <li><a href="/products">Hiking shoes</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.yamm-content -->
                        </li>
                    </ul>
                </li>

                <li class="dropdown yamm-fw">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="200">Template <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <h5>Shop</h5>
                                        <ul>
                                            <li><a href="/">Homepage</a>
                                            </li>
                                            <li><a href="/products">Category - sidebar left</a>
                                            </li>
                                            <li><a href="category-right.html">Category - sidebar right</a>
                                            </li>
                                            <li><a href="category-full.html">Category - full width</a>
                                            </li>
                                            <li><a href="detail.html">Product detail</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>User</h5>
                                        <ul>
                                            <li><a href="/register">Register / login</a>
                                            </li>
                                            <li><a href="/customer-orders">Orders history</a>
                                            </li>
                                            <li><a href="/customer-order">Order history detail</a>
                                            </li>
                                            <li><a href="/customer-wishlist">Wishlist</a>
                                            </li>
                                            <li><a href="/customer-account">Customer account / change password</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Order process</h5>
                                        <ul>
                                            <li><a href="/cart">Shopping cart</a>
                                            </li>
                                            <li><a href="/checkout1">Checkout - step 1</a>
                                            </li>
                                            <li><a href="/checkout2">Checkout - step 2</a>
                                            </li>
                                            <li><a href="/checkout3">Checkout - step 3</a>
                                            </li>
                                            <li><a href="/checkout4">Checkout - step 4</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-sm-3">
                                        <h5>Pages and blog</h5>
                                        <ul>
                                            <li><a href="blog.html">Blog listing</a>
                                            </li>
                                            <li><a href="post.html">Blog Post</a>
                                            </li>
                                            <li><a href="faq.html">FAQ</a>
                                            </li>
                                            <li><a href="text.html">Text page</a>
                                            </li>
                                            <li><a href="text-right.html">Text page - right sidebar</a>
                                            </li>
                                            <li><a href="404.html">404 page</a>
                                            </li>
                                            <li><a href="/contact">Contact</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.yamm-content -->
                        </li>
                    </ul>
                </li>
            </ul>

        </div>
        <!--/.nav-collapse -->

        <div class="navbar-buttons">

            <div class="navbar-collapse collapse right" id="basket-overview">
                <a href="/cart/view" class="btn btn-primary navbar-btn"><i class="fa fa-shopping-cart"></i><span class="hidden-sm">3 items in cart</span></a>
            </div>
            <!--/.nav-collapse -->

            <div class="navbar-collapse collapse right" id="search-not-mobile">
                <button type="button" class="btn navbar-btn btn-primary" data-toggle="collapse" data-target="#search">
                    <span class="sr-only">Toggle search</span>
                    <i class="fa fa-search"></i>
                </button>
            </div>

        </div>

        <div class="collapse clearfix" id="search">

            <form class="navbar-form" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-btn">

                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>

                        </span>
                </div>
            </form>

        </div>
        <!--/.nav-collapse -->

    </div>
    <!-- /.container -->
</div>
<!-- /#navbar -->

<!-- *** NAVBAR END *** -->