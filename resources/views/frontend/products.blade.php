@extends('layouts.app')

@section('content')

        <div id="content">
            <div class="container">

                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Home</a>
                        </li>
                        <li>Ladies</li>
                    </ul>
                </div>

                <div class="col-md-3">
                    <!-- *** MENUS AND FILTERS *** -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Categories</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                <li>
                                    <a href="products.blade.php">Men <span class="badge pull-right">42</span></a>
                                    <ul>
                                        <li><a href="products.blade.php">T-shirts</a>
                                        </li>
                                        <li><a href="products.blade.php">Shirts</a>
                                        </li>
                                        <li><a href="products.blade.php">Pants</a>
                                        </li>
                                        <li><a href="products.blade.php">Accessories</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="active">
                                    <a href="products.blade.php">Ladies  <span class="badge pull-right">123</span></a>
                                    <ul>
                                        <li><a href="products.blade.php">T-shirts</a>
                                        </li>
                                        <li><a href="products.blade.php">Skirts</a>
                                        </li>
                                        <li><a href="products.blade.php">Pants</a>
                                        </li>
                                        <li><a href="products.blade.php">Accessories</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="products.blade.php">Kids  <span class="badge pull-right">11</span></a>
                                    <ul>
                                        <li><a href="products.blade.php">T-shirts</a>
                                        </li>
                                        <li><a href="products.blade.php">Skirts</a>
                                        </li>
                                        <li><a href="products.blade.php">Pants</a>
                                        </li>
                                        <li><a href="products.blade.php">Accessories</a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>

                        </div>
                    </div>

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Brands <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Armani (10)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Versace (12)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Carlo Bruni (15)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox">Jack Honey (14)
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>

                            </form>

                        </div>
                    </div>

                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading">
                            <h3 class="panel-title">Colours <a class="btn btn-xs btn-danger pull-right" href="#"><i class="fa fa-times-circle"></i> Clear</a></h3>
                        </div>

                        <div class="panel-body">

                            <form>
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour white"></span> White (14)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour blue"></span> Blue (10)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour green"></span> Green (20)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour yellow"></span> Yellow (13)
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> <span class="colour red"></span> Red (10)
                                        </label>
                                    </div>
                                </div>

                                <button class="btn btn-default btn-sm btn-primary"><i class="fa fa-pencil"></i> Apply</button>

                            </form>

                        </div>
                    </div>

                    <!-- *** MENUS AND FILTERS END *** -->

                    <div class="banner">
                        <a href="#">
                            <img src="images/banner.jpg" alt="sales 2014" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="col-md-9">
                    {{--<div class="box">--}}
                        {{--<h1>Ladies</h1>--}}
                        {{--<p>In our Ladies department we offer wide selection of the best products we have found and carefully selected worldwide.</p>--}}
                    {{--</div>--}}

                    <div class="box info-bar">
                        <div class="row">
                            <div class="col-sm-12 col-md-4 products-showing">
                                Showing <strong>{{ $products->count() }}</strong> of <strong> {{ $products->total() }}</strong> Products
                            </div>

                            <div class="col-sm-12 col-md-8  products-number-sort">
                                <div class="row">
                                    <form class="form-inline" action="{{ url()->current() }}" method="get">
                                        <div class="col-md-4 col-sm-4">
                                            <strong>Page No.:</strong>
                                            <a href="#" class="btn btn-default btn-sm btn-primary">1</a>
                                            {{--<div class="products-number">--}}
                                                {{--<strong>Show</strong>  <a href="#" class="btn btn-default btn-sm btn-primary">12</a>  <a href="#" class="btn btn-default btn-sm">24</a>  <a href="#" class="btn btn-default btn-sm">All</a> products--}}
                                            {{--</div> --}}
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-sort-by">
                                                <strong>Sort by</strong>
                                                <select name="sort" class="form-control">
                                                    <option value="1" @if(isset($_GET['sort'])) @if($_GET['sort']==1) {{ 'selected' }} @endif @endif>Price Ascending</option>
                                                    <option value="2" @if(isset($_GET['sort'])) @if($_GET['sort']==2) {{ 'selected' }} @endif @endif>Price Descending</option>
                                                    <option value="3" @if(isset($_GET['sort'])) @if($_GET['sort']==3) {{ 'selected' }} @endif @endif>Latest Entry First</option>
                                                    <option value="4" @if(isset($_GET['sort'])) @if($_GET['sort']==4) {{ 'selected' }} @endif @endif>Oldest Entry First</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-2">
                                            <input type="submit" class="btn" value="Sort">
                                        </div>
                                        {!! csrf_field() !!}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="row products">
                    @if(count($products) > 0)
                        @foreach($products as $product)
                            <div class="col-md-4 col-sm-6">
                                <div class="product">
                                    <div class="flip-container">
                                        <div class="flipper">
                                            <div class="front">
                                                <a href="{{route('product.single','$product->slug')}}">
                                                    <img src="{{asset('images/products/'.$product->image1)}}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="back">
                                                <a href="{{url('product/'.$product->slug)}}">
                                                    <img src="{{asset('images/products/'.$product->image2)}}" alt="" class="img-responsive">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{url('/'.$product->slug)}}" class="invisible">
                                        <img src="images/product1.jpg" alt="" class="img-responsive">
                                    </a>
                                    <div class="text">
                                        <h3><a href="{{url('product/'.$product->slug)}}">{{$product->productName}}</a></h3>
                                        <p class="price">{{$product->productPrice}}</p>
                                        <p class="buttons">
                                            <a href="{{url('product/'.$product->slug)}}" class="btn btn-default">View detail</a>
                                            <a href="basket.html" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                        </p>
                                    </div>
                                    <!-- /.text -->
                                </div>
                                <!-- /.product -->
                            </div>
                        @endforeach
                    @endif




                        <!-- /.col-md-4 -->
                    </div>
                    @if(isset($_GET['sort'])) @php $sort=$_GET['sort']; @endphp @else @php $sort=5; @endphp @endif
                    <div class="pages">{{$products->appends(['sort' => $sort])->links()}}</div>
                    <!-- /.products -->

                    {{--<div class="pages">--}}

                        {{--<p class="loadMore">--}}
                            {{--<a href="#" class="btn btn-primary btn-lg"><i class="fa fa-chevron-down"></i> Load more</a>--}}
                        {{--</p>--}}

                        {{--<ul class="pagination">--}}
                            {{--<li><a href="#">&laquo;</a>--}}
                            {{--</li>--}}
                            {{--<li class="active"><a href="#">1</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">2</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">3</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">4</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">5</a>--}}
                            {{--</li>--}}
                            {{--<li><a href="#">&raquo;</a>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</div>--}}


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
@endsection