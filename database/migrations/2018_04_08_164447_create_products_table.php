<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('products', function (Blueprint $table) {
            $table->increments('productId');
            $table->string('productName');
            $table->string('slug');
            $table->text('productDescription');
            $table->text('productNote');
            $table->integer('categoryId')->unsigned();
            $table->foreign('categoryId')
                 ->references('categoryId')
                 ->on('categories')
                 ->onDelete('cascade')
                 ->onUpdate('cascade');
            $table->integer('brandId')->unsigned();
            $table->foreign('brandId')
                 ->references('brandId')
                 ->on('brands')
                 ->onDelete('cascade')
                 ->onUpdate('cascade');
            $table->integer('genderId');
            $table->string('productPrice');
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('products');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
