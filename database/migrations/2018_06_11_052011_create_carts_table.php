<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')
                 ->references('productId')
                 ->on('products')
                 ->onDelete('cascade')
                 ->onUpdate('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                 ->references('id')
                 ->on('users')
                 ->onDelete('cascade')
                 ->onUpdate('cascade');
            $table->integer('size_id')->unsigned();
            $table->foreign('size_id')
                 ->references('id')
                 ->on('sizes')
                 ->onDelete('cascade')
                 ->onUpdate('cascade');
            $table->integer('color_id')->unsigned();
            $table->foreign('color_id')
                 ->references('id')
                 ->on('colors')
                 ->onDelete('cascade')
                 ->onUpdate('cascade');
            $table->integer('quantity')->default(1);
            $table->integer('price');
            $table->boolean('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
